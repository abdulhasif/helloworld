package com.learning.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class HelloSpringBoot {
	
	@GetMapping("/hello")
	String displayHello()
	{
		return "Hello World !!!";
	}
	public static void main(String args[])
	{
		SpringApplication.run(HelloSpringBoot.class, args);
	}

}
